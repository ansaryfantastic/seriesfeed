<html>
    <head>
        <style>
            .ui-progressbar {
                position: relative;
            }
            .progress-label{
                background-color:red;
                position: absolute;
                left: 50%;
                top: 4px;
                font-weight: bold;
                text-shadow: 1px 1px 0 #fff;
            }
            .progressHead{
                background-color:#787878 ;		

            }


        </style>

        <title>ShowRSS</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
        <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script type="text/javascript" src="js/jquery.slimscroll.js"></script>

        <script src="js/jquery.bpopup.min.js"></script>

        <script type="text/javascript">
            function getNextNo(startIndex, str) {
                var ret = [];
                var j = startIndex;
                var no = "";
                while (str[j] !== ' ') {
                    no += str[j]; // 55
                    j++;
                }

                ret['number'] = parseInt(no);
                ret['index'] = j + 1;

                return ret;
            }
            var isOpenPopup2 = false;
            var requestNo = 0;

            function showPopup(e) {
                var showName = document.forms['add-show-form']['showname'].value;

                $('<a>', {
                    class: 'b-close',
                    text: 'X'
                }).appendTo('#___popup');

                $('<div>', {
                    id: 'popup_extras'
                }).appendTo('#___popup');

                $('<div>', {
                    class: 'container'
                }).appendTo('#___popup');


                $('<div>', {
                    class: 'pb_content'
                }).appendTo('.container');

                $('<div>', {
                    class: 'circle'
                }).appendTo('.pb_content');

                $('<div>', {
                    class: 'circle1'
                }).appendTo('.pb_content');
                e.preventDefault();

                $('#___popup').bPopup({
                    modalClose: false,
                    loadUrl: "get-show-info.php",
                    loadData: {showname: showName},
                    loadCallback: function() {
                        console.log("Inside loadCallback");

                        $(".container").remove();

                        var _showlist = jQuery("#show_list");

                        _showlist.accordion({collapsible: true, heightStyle: "content"});
                        _showlist.slimScroll({color: "#000099"});

                        $("#btn_subscribe").bind('click', function(e) {
                            console.log("Subscribe button clicked");

                            $('#___popup').bPopup().close();

                            var showids = [0];
                            var frm = document.forms["frm_subscription"].subscription_list;
                            var nShows = frm.length;

                            if (nShows === 'undefined')
                                nShows = 1;
                            for (var i = 0; i < nShows; i++) {
                                if (frm[i].checked) {
                                    //alert("Popup: " + isOpenPopup2);
                                    if (isOpenPopup2 === false) {
                                        showPopup2(e);
                                        isOpenPopup2 = true;
                                    }
                                    else {
                                        console.log("Already open");
                                    }
                                    showids[i] = frm[i].id;
                                    console.log(showids[i]);
                                    requestNo = showids[i];
                                    
                                    console.log("REQ 1: Sending request number: " + requestNo);
                                    
                                    addNewProgressBar("#progress_list", "bar" + requestNo);


                                    $.ajax({
                                        url: 'get-episode-info-from-show-id.php',
                                        data: {showid: showids[i], reqid: requestNo},
                                        type: 'POST',
                                        success: function(data) {
                                            var len = data.length;
                                            var x = getNextNo(0, data);

                                            var d = x['number'];
                                            console.log("Request " + d + " completed!");
                                            progress($('#bar' + d));

                                            // updatePB($('#bar' + d), data);

                                            var episodes = [];
                                            var j = parseInt(x['index']);
                                            var total = 0;
                                            while (j < len) {
                                                var s = getNextNo(j, data);
                                                var e = getNextNo(s['index'], data);
                                                j = parseInt(e['index']);
                                                episodes[parseInt(s['number'])] = parseInt(e['number']);

                                                total += parseInt(e['number']);
                                            }

                                            var sno = 1;
                                            var percentComplete = 0;
                                            var completedCount = 0;
                                            for (var n in episodes) {
                                                var ne = episodes[n];
                                                for (var eno = 1; eno <= ne; eno++) {
                                                    console.log("REQ 2: " + d);
                                                    $.ajax({
                                                        url: 'download-ep-data.php',
                                                        data: {showid: d, seasonno: sno, episodeno: eno},
                                                        type: 'POST',
                                                        success: function(data) {
                                                            percentComplete = (5 + (++completedCount / total) * 95);
                                                            console.log("REQ 2: " + d + ". Completed " + percentComplete + " percent!");
                                                            $('#bar' + d).progressbar("value",  Math.ceil(percentComplete));
                                                        }

                                                    });
                                                }
                                                sno++;
                                            }

                                        }

                                    });


                                }
                            }
                            e.preventDefault();
                        });
                    }
                });
            }

            $(document).ready(function() {
                $("#add_show_btn").bind('click', function(e) {
                    showPopup(e);
                });

                $("#add-show-form").keypress(function(event) {
                    if (event.which === 13) {
                        showPopup(event);
                        event.preventDefault();
                    }
                });

                $(".username").click(function() {
                    $("#err-user").slideUp("fast");
                });

                $(".password").click(function() {

                    $("#err-pass").slideUp("fast");
                });

                $("#reg").click(function() {
                    $("#panel2").slideToggle("slow");
                    $("#panel").slideUp("slow");
                });

                $("#log").click(function() {
                    $("#panel").slideToggle("slow");
                    $("#panel2").slideUp("slow");
                });
            });

            function validateLogin()
            {
                var w = document.forms["login-form"]["username"].value;
                var x = document.forms["login-form"]["passwd"].value;

                if (w === null || w === "")
                {
                    $(document).ready(function() {
                        $("#err-user").slideDown("fast");

                    });
                    return false;

                }

                if (x === null || x === "")
                {
                    $(document).ready(function() {
                        $("#err-pass").slideDown("fast");

                    });
                    return false;
                }

            }

            function validateReg()
            {
                var w = document.forms["reg-form"]["username"].value;
                var x = document.forms["reg-form"]["email"].value;
                var y = document.forms["reg-form"]["password"].value;
                var z = document.forms["reg-form"]["con-password"].value;

                var atpos = x.indexOf("@");
                var dotpos = x.lastIndexOf(".");



                if (w === null || w === "")
                {
                    alert("Enter Your Username");
                    return false;
                }


                else if (atpos < 1 || (dotpos < atpos + 2) || dotpos + 2 >= x.length)
                {
                    alert("Enter a valid e-mail address");
                    return false;
                }

                else if (y === null || y === "")
                {
                    alert("Enter Password");
                    return false;
                }
                else if (z === null || z === "")
                {
                    alert("Confirm Password");
                    return false;
                }
                else if (y !== z)
                {
                    alert("Password doesn't match!!");
                    //document.reg-form.y.value = "";
                    //document.reg-form.z.value = "";
                    //document.reg-form.y.focus();
                    return false;
                }
            }

            var ctr = 0;


            function progress(progressbar) {
                // console.log("Inside method progress");
                var val = progressbar.progressbar("value") || 4;
                progressbar.progressbar("value", val + 1);
            }
            var isOpen = false;
            function addNewProgressBar(divname, progressBarId) {
                ctr++;

                $("<div>", {
                    id: progressBarId
                }).appendTo(divname);

                $("<div>", {
                    text: 'Loading...' + progressBarId,
                    id: 'pb_label' + ctr,
                    class: 'progressHead'
                }).appendTo('#' + progressBarId);

                var progressbar = $('#' + progressBarId);
                var progressLabel = $('#pb_label' + ctr);

                progressbar.progressbar({
                    value: false,
                    change: function() {
                        progressLabel.text(progressBarId + " loaded " + progressbar.progressbar("value") + "%");
                    },
                    complete: function() {
			
                        progressLabel.text("Complete!");
						);
                    }
                });


                //progress(progressbar);

            }

            function removeProgressBar(pb_id) {
                var pb = $('#bar' + pb_id);
                pb.remove();
            }

            function showPopup2(evt) {

                isPopupShowing = true;
                var w = window.innerWidth - 320;
                var h = window.innerHeight - 290;

                $("#progress_popup").bPopup({
                    modal: false,
                    modalClose: false,
                    position: [w, h]
                });

                if (typeof evt !== 'undefined')
                    evt.preventDefault();
            }
            ;

            function closePopup() {
                isPopupShowing = false;
                $("#progress_popup").bPopup().close();
            }

            jQuery("document").ready(function() {
                $('#progress_list').slimScroll({height: 150});

                $(window).resize(function() {
                    console.log("Resize called");
                    if (isPopupShowing) {
                        setTimeout(function() {
                            closePopup();
                            showPopup2();
                        }, 150);
                    }
                });

                $('<a>', {
                    class: 'b-close',
                    text: 'X'
                }).appendTo('#progress_popup');



            });


        </script>
    </head>

    <body>
        <div class="main">
            <div class="header">
                <div class="logo">
                    <a href="showrss.php"> <img src="includes/logo2.png" />
                    </a>

                </div>
                <?php
                session_start();

                if (ISSET($_SESSION ['username'])) {
                    echo "<div class='logged'>";

                    echo "<div id='logname'>";
                    // echo "<a href=''>$_SESSION['username']</a>";

                    echo "Hello,";

                    echo "<span style='color:8F3A9E'>";
                    echo $_SESSION ['username'];
                    echo "</span>";
                    echo "</div>";
                    echo "<div id='logout'>";
                    echo "<a href='logout.php'>Logout</a>";
                    echo "</div>";
                    // echo "</div>";
                } else {
                    echo "<div class='login'>";
                    echo "<div id='log'>";
                    echo "<a href='#log'>Login</a>";
                    echo "</div>";
                    echo "<div id='reg'>";
                    echo "<a href='#reg'>Register</a>";
                    echo "</div>";
                    // echo "</div>";
                }
                ?>
            </div>
        </div>

        <div id="___popup"></div>

        <div class="mainmenu" style="width:100%">
            <ul>
                <li class = 'mli'><a href="schedule.php">Schedule</a></li>
                <li class = 'mli'> <a href="feeds.php">Feeds</a></li>
                <li class = 'mli'><a href="browse.php">Browse</a></li>
                <li class = 'mli'><a href="faq.php">F.A.Q</a></li>
                <li class = 'mli'><a href="contact.php">Contact us</a></li>
            </ul>
        </div>

        <div id="panel">

            <form name="login-form" onsubmit="return validateLogin()"
                  action="check_login.php" method="post">
                <input class="username" type="text" name="username"
                       placeholder="Username"><br />
                <p id="err-user">Please Enter your username</p>


                <p>
                    <input class="password" type="password" name="passwd"
                           placeholder="Password"><br />
                </p>


                <p id="err-pass">Please Enter your password
                </p>


                <p>

                    <input class="submit" type="submit" value="Login"> <a
                        class="forpass" href="">Forgot Password?</a>
                </p>

            </form>
        </div>

        <div id="panel2">

            <form name="reg-form" onsubmit="return validateReg()"
                  action="registration.php" method="post">
                <input class="username" type="text" name="username"
                       placeholder="Username"><br /> <input class="email" type="text"
                       name="email" placeholder="Email Address"><br /> <input
                       class="password" type="password" name="password"
                       placeholder="Password"><br /> <input class="con-password"
                       type="password" name="con-password" placeholder="Confirm Password"><br />

                <input class="create-account" type="submit" value="Create Account">
            </form>
        </div>
        <?php
        if (ISSET($_SESSION ['username'])) {
            echo "<div class='show_add'>";
            echo "<div class='add_show'>";
            echo "<form action='addShowToList.php' method ='get'>
				<span>Pick a Show:&nbsp</span> <select name='browse_show'
					id='br_box'>
					<option value=''>Pick a show:</option>";

            include ('userAvailableShows.php');

            echo "</select> <input id='show_info' type='submit' class='home_page_btn'
					value='Add To List'>";

            echo "</form id='frm_add_show'>";

            echo "</div>";

            echo "<div class='add_showName'>";
            echo "<form autocomplete='off' id=\"add-show-form\">
				<span> <b>OR</b>,&nbsp Add a Show:&nbsp</span> 
					<input id='showname' class='addShowName' type='text' value='Sherlock'
					placeholder='Enter Show Name'>";

            echo "</select> <input id='add_show_btn' type='button' class='home_page_btn'
					value='Add'>";

            echo "</form>";

            echo "</div>";

            echo "</div>";

            echo "<div class='registered_show'>";
            echo "<div class='yourshow'>";
            echo "<h2>&nbsp;My Shows</h2>";
            echo "<ul>";
            include 'yourShowNames.php';
            echo "</ul>";
            echo "</div>";
            echo "<div class='yourshowlist'>";
            include ('yourshowlist.php');
            echo "</div>";

            echo "</div>";
        }
        ?>
        <div class="maincontent">
            <div class="sidebar">
                <h2>Latest Added Shows</h2>
                <ul> 
                    <?php
                    include ('latest_shows.php');
                    ?>
                </ul>
            </div>

            <div class="content">
                <h2>Recently Aired Shows</h2>

                <ul>
                    <?php
                    include ('recently_aired.php');
                    ?>
                </ul>

            </div>

        </div>


        <div id="progress_popup" style="display:none;min-width:300px; max-width:300px; min-height:150px; background-color:rgb(199, 225, 225);border-radius:5px">		
            <p style="text-align:center;margin:5px auto"> &nbsp;</p>
            <div id="progress_list" style="overflow:auto; max-height:150px; max-width:300px;"></div>
        </div>	
        <div class="footer">
            <p align="right"></p>
        </div>
    </body>
</html>
