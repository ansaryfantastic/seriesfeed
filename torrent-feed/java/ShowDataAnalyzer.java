import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ShowDataAnalyzer {
	private static final String API_KEY = "9457537B50F074B8";
	private static final SAXParserFactory factory = SAXParserFactory
			.newInstance();

	public static String[] getShowInfoFromMagnetLink(String showdata,
			String showname) throws UnsupportedEncodingException {
		showdata = URLDecoder.decode(showdata, "utf-8");

		String pattern = "([0-9]{1,2}x[0-9]{1,2})|((S|s)[0-9]{1,2}(E|e)[0-9]{1,2})|(season( )*[0-9]{1,2}( )*episode( )*[0-9]{1,2})";
		Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);

		String[] data = new String[4];

		Matcher m = r.matcher(showdata);

		if (m.find()) {
			int startIndex = m.start();
			int endIndex = m.end();

			String se = m.group();
			Pattern sepat = Pattern.compile("[0-9]+");
			m = sepat.matcher(se);

			if (m.find()) {
				data[1] = m.group();
				m.find(m.end());
				data[2] = m.group();
			} else {
				return null;
			}

			data[0] = capitalizeString(showdata.substring(0, startIndex));

			data[3] = showdata.substring(endIndex);

			Pattern pat = Pattern
					.compile("(xvid)|(720p)|(1080p)|(480p)|(x264)|(h264)");

			m = pat.matcher(data[3]);

			if (m.find())
				data[3] = m.group();
			else {
				return null;
			}

		} else {
			return null;
		}

		return data;
	}

	private static int REQUEST_NO = 0;

	public Vector<String[]> getSeriesInfo(String seriesid)
			throws ParserConfigurationException, SAXException, IOException {
		String url = "http://thetvdb.com/api/" + API_KEY + "/series/"
				+ seriesid + "/all/en.xml";
		SAXParser saxParser = factory.newSAXParser();

		final Vector<String[]> vec = new Vector<String[]>();

		DefaultHandler episodeHandler = new DefaultHandler() {
			private boolean episode = false;
			private boolean episodeId = false;
			private boolean seasonNo = false;
			private boolean episodeNo = false;
			private boolean episodeName = false;
			private boolean overview = false;

			String[] info = new String[5];
			private int totMatched = 0;

			@Override
			public void startElement(String uri, String localName,
					String qName, Attributes attributes) throws SAXException {
				if (qName.equalsIgnoreCase("Episode")) {
					episode = true;
				}
				if (episode) {
					if (qName.equalsIgnoreCase("id")) {
						episodeId = true;
					} else if (qName.equalsIgnoreCase("EpisodeName")) {
						episodeName = true;
					} else if (qName.equalsIgnoreCase("EpisodeNumber")) {
						episodeNo = true;
					} else if (qName.equalsIgnoreCase("SeasonNumber")) {
						seasonNo = true;
					} else if (qName.equalsIgnoreCase("Overview")) {
						overview = true;
					}
				}
			}

			@Override
			public void characters(char[] ch, int start, int length)
					throws SAXException {
				if (episode) {
					if (episodeId) {
						info[0] = new String(ch, start, length);
						episodeId = false;
						totMatched++;
					} else if (seasonNo) {
						info[1] = new String(ch, start, length);
						seasonNo = false;
						totMatched++;
					} else if (episodeNo) {
						info[2] = new String(ch, start, length);
						episodeNo = false;
						totMatched++;
					} else if (episodeName) {
						info[3] = new String(ch, start, length);
						episodeName = false;
						totMatched++;
					} else if (overview) {
						info[4] = new String(ch, start, length);
						overview = false;
						totMatched++;
					} else if (totMatched >= 5) {
						vec.add(info);
						info = new String[5];
						totMatched = 0;
					}
				}
			}

			@Override
			public void endElement(String uri, String localName, String qName)
					throws SAXException {
				if (qName.equalsIgnoreCase("Episode")) {
					episode = false;
				}
			}

		};

		saxParser.parse(url, episodeHandler);

		return vec;
	}

	public Vector<String[]> getPossibleTVShowsFromTVDB(String showname)
			throws Exception {

		System.out.println("Request no " + ++REQUEST_NO
				+ " received to find out information about " + showname);
		SAXParser saxParser = factory.newSAXParser();
		final Vector<String[]> vec = new Vector<String[]>();

		DefaultHandler showNameHandler = new DefaultHandler() {
			boolean seriesId = false;
			boolean seriesName = false;
			boolean overview = false;
			boolean firstAired = false;

			int totMatched = 0;
			String[] info = new String[4];

			@Override
			public void startElement(String uri, String localName,
					String qName, Attributes attributes) throws SAXException {
				if (qName.equals("seriesid")) {
					seriesId = true;
				} else if (qName.equalsIgnoreCase("seriesname")) {
					seriesName = true;
				} else if (qName.equalsIgnoreCase("overview")) {
					overview = true;
				} else if (qName.equalsIgnoreCase("firstaired")) {
					firstAired = true;
				}
			}

			@Override
			public void characters(char[] ch, int start, int length)
					throws SAXException {
				if (seriesId) {
					info[0] = new String(ch, start, length);
					seriesId = false;
					totMatched++;
				}

				if (seriesName) {
					info[1] = new String(ch, start, length);
					seriesName = false;
					totMatched++;
				}

				if (overview) {
					info[2] = new String(ch, start, length);
					overview = false;
					totMatched++;
				}

				if (firstAired) {
					info[3] = new String(ch, start, length);
					firstAired = false;
					totMatched++;
				}

				if (totMatched >= 4) {
					vec.add(info);
					info = new String[4];
					totMatched = 0;
				}
			}
		};

		saxParser.parse("http://www.thetvdb.com/api/GetSeries.php?seriesname="
				+ URLEncoder.encode(showname, "utf-8"), showNameHandler);

		System.out.println("Processing completed! Sending reply... "
				+ vec.size() + "\n");

		return vec;
	}

	public static final String JAVABRIDGE_PORT = "8080";
	static final php.java.bridge.JavaBridgeRunner runner = php.java.bridge.JavaBridgeRunner
			.getInstance(JAVABRIDGE_PORT);

	public static void main(String[] args) throws Exception {
		// 82459
		// ShowDataAnalyzer analyzer = new ShowDataAnalyzer();
		// Vector<String[]> v = analyzer
		// .getPossibleTVShowsFromTVDB("X-Men");
		//
		// for (String[] s : v) {
		// for (String a : s) {
		// System.out.println(a);
		// }
		// System.out.println();
		// }
		//
		// Vector<String[]> v = analyzer.getSeriesInfo("82870");
		// for (String[] s : v) {
		// System.out.println("Episode ID: " + s[0]);
		// System.out.println("Season No: " + s[1]);
		// System.out.println("Episode No: " + s[2]);
		// System.out.println("Episode Name: " + s[3]);
		// System.out.println("Overview: " + s[4]);
		//
		// System.out.println();
		// }

		runner.waitFor();
		System.exit(0);
	}

	public static String capitalizeString(String string) {
		char[] chars = string.toLowerCase().toCharArray();
		boolean found = false;
		for (int i = 0; i < chars.length; i++) {
			if (!found && Character.isLetter(chars[i])) {
				chars[i] = Character.toUpperCase(chars[i]);
				found = true;
			} else if (Character.isWhitespace(chars[i]) || chars[i] == '.'
					|| chars[i] == '\'') {
				found = false;
			}
		}
		return String.valueOf(chars);
	}
}
