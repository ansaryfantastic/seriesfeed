<html>
	<head>
		<title>ShowRSS</title>
		<link href="style.css" rel="stylesheet" type="text/css"/>
		<script src="jquery-1.10.2.js">
		</script>
		<script> 
			$(document).ready(function(){
			  $("#log").click(function(){
				$("#panel").slideToggle("slow");
				$("#panel2").slideUp("slow");
			  });
			});
			$(document).ready(function(){
			  $("#reg").click(function(){
				$("#panel2").slideToggle("slow");
				$("#panel").slideUp("slow");
			  });
			});
			
			$(document).ready(function(){
			  $(".username").click(function(){
				
				$("#err-user").slideUp("fast");
			  });
			});
			$(document).ready(function(){
			  $(".password").click(function(){
				
				$("#err-pass").slideUp("fast");
			  });
			});
		</script>
		<script>
			function validateLogin()
			{
			var w=document.forms["login-form"]["username"].value;
			var x=document.forms["login-form"]["passwd"].value;
			
			if (w==null || w=="")
			  {
			  $(document).ready(function(){
				$("#err-user").slideDown("fast");
			  
				});
			  return false;
			  
			  }
			
			if (x==null || x=="")
			  {
			  $(document).ready(function(){
				$("#err-pass").slideDown("fast");
			  
				});
			  return false;
			  }
			
			}
		</script>
		<script>
			function validateReg()
			{
			var w=document.forms["reg-form"]["username"].value;
			var x=document.forms["reg-form"]["email"].value;
			var y=document.forms["reg-form"]["password"].value;
			var z=document.forms["reg-form"]["con-password"].value
			
			var atpos=x.indexOf("@");
			var dotpos=x.lastIndexOf(".");
			


			if (w==null || w=="")
			  {
			  alert("Enter Your Username");
			  return false;
			  }
			  
				
			else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
				  {
				alert("Enter a valid e-mail address");
				return false;
			  }
			  
			else if (y==null || y=="")
			  {
			  alert("Enter Password");
			  return false;
			  }
			else if (z==null || z=="")
			  {
			  alert("Confirm Password");
			  return false;
			  }
			else if(y!=z)
			  {
			  alert("Password and Conform Password field must be same!");
			  //document.reg-form.y.value = "";
			  //document.reg-form.z.value = "";
			  //document.reg-form.y.focus();
			  return false;
			  }
			
			}
		</script>
	</head>
	
	<body>
		
		<div class="main">
		    <div class= "header">
				<div class="logo">
				   <a href="showrss.php">	<img src="includes/logo2.png" /> <a>
				</div>
				<div class="login">
					
						<div id="log"> <a href="#log">Login</a></div>
						<div id="reg"> <a href="#reg">Register</a></div>
						
					
					
				</div>
			</div>
			<div class="mainmenu">
				<ul>
					<li><a href="schedule.php">Schedule</a></li>
					<li><a href="feeds.php">Feeds</a></li>
					<li><a href="browse.php">Browse</a></li>
					<li><a href="faq.php">F.A.Q</a></li>
					<li><a href="contact.php">Contact us</a></li>
				</ul>
			</div>
			
			<div id="panel">
			
				<form name="login-form" onsubmit="return validateLogin()" action="registration.php" method="post">
					<input class="username" type="text" name="username" placeholder="Username" ></br>
					<p id="err-user">Please Enter your username <p>
					<input class="password" type="password" name="passwd" placeholder="Password"></br>
					<p id="err-pass">Please Enter your password <p>
					
					<input class="submit" type="submit" value="Login"> <a class="forpass" href="">Forgot Password?</a>
				</form>
			</div>
			
			<div id="panel2">
			
				<form name="reg-form" onsubmit="return validateReg()" action="registration.php" method="post">
					<input class="username" type="text" name="username" placeholder="Username" ></br>
					<input class="email" type="text" name="email" placeholder="Email Address" ></br>
					<input class="password" type="password" name="password" placeholder="Password"></br>
					<input class="con-password" type="password" name="con-password" placeholder="Confirm Password"></br>
					
					<input class="create-account" type="submit" value="Create Account">
				</form>
			</div>
			
			<div class="maincontent">
				<div class="sidebar">
					<h2>Latest Added Shows</h2>
					<ul>
						<li><a href="">Hulk and the Agents of S.M.A.S.H.</a></li>
						<li><a href="">Avengers Assemble (2013)</a></li>
						<li><a href="">Almost Human</a></li>
						<li><a href="">Citizen Khan</a></li>
						<li><a href="">The Escape Artist</a></li>
						<li><a href="">Ambassadors</a></li>
					</ul>
				</div>
				<div class="content">
					<h2 >Recently Aired Shows</h2>
					
					<ul>
						<li><a href="">Hulk and the Agents of S.M.A.S.H.</a></li>
						<li><a href="">Avengers Assemble (2013)</a></li>
						<li><a href="">Almost Human</a></li>
						<li><a href="">Citizen Khan</a></li>
						<li><a href="">The Escape Artist</a></li>
						<li><a href="">Ambassadors</a></li>
					</ul>
					
				</div>
				
			</div>
			<div class="footer">
				<p align="right"></p>
			</div>
		</div>

	</body>
</html>
