<html>
    <head>
        <title>ShowRSS</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css" />

				<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
				<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
				<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
				<script type="text/javascript" src="js/jquery.slimscroll.js"></script>
  
        <script src="js/jquery.bpopup.min.js"></script>
        
        <script type="text/javascript">
		      	function showPopup(e){
        		    var showName = document.forms['add-show-form']['showname'].value;
   
                    $('<a>', {
                        class: 'b-close',
                        text: 'X'
                    }).appendTo('#___popup');
                    
                    $('<div>', {
                    	id: 'popup_extras'
                    }).appendTo('#___popup');

                    $('<div>', {
                        class: 'container'
                    }).appendTo('#___popup');


                    $('<div>', {
                        class: 'pb_content'
                    }).appendTo('.container');

                    $('<div>', {
                        class: 'circle'
                    }).appendTo('.pb_content');

                    $('<div>', {
                        class: 'circle1'
                    }).appendTo('.pb_content');
					e.preventDefault();
					
					$('#___popup').bPopup({
						modalClose: false,
						loadUrl: "get-show-info.php",
						loadData: {showname: showName},
						loadCallback: function() {
							console.log("Inside loadCallback");
						
							$(".container").remove();
							
							var _showlist = jQuery("#show_list");
							
							
							_showlist.accordion({collapsible:true, heightStyle:"content"});
							_showlist.slimScroll({color:"#DC143C"});
							
							jQuery("#btn_subscribe").bind('click', function(e){								

								
								$('#___popup').bPopup().close();
								var ctr = 0;
								var checked_showids = [];
								var frm = document.forms["frm_subscription"].subscription_list;
								var nShows = frm.length;
								for (var i = 0; i < nShows; i++){
									if (frm[i].checked){
										checked_showids[++ctr] = frm[i].id;
									}
								}
								
								console.log(checked_showids.length);
								
								e.preventDefault();
							});
						}
					});
        	}
        
            $(document).ready(function() {
                $("#add_show_btn").bind('click', function(e) {
					showPopup(e);  
                });
                
                $("#add-show-form").keypress(function(event){
                	if (event.which == 13){
						showPopup(event);
	                	event.preventDefault();
                	}
                });

                $(".username").click(function() {
                    $("#err-user").slideUp("fast");
                });

                $(".password").click(function() {

                    $("#err-pass").slideUp("fast");
                });

                $("#reg").click(function() {
                    $("#panel2").slideToggle("slow");
                    $("#panel").slideUp("slow");
                });

                $("#log").click(function() {
                    $("#panel").slideToggle("slow");
                    $("#panel2").slideUp("slow");
                });
            });

            function validateLogin()
            {
                var w = document.forms["login-form"]["username"].value;
                var x = document.forms["login-form"]["passwd"].value;

                if (w == null || w == "")
                {
                    $(document).ready(function() {
                        $("#err-user").slideDown("fast");

                    });
                    return false;

                }

                if (x == null || x == "")
                {
                    $(document).ready(function() {
                        $("#err-pass").slideDown("fast");

                    });
                    return false;
                }

            }

            function validateReg()
            {
                var w = document.forms["reg-form"]["username"].value;
                var x = document.forms["reg-form"]["email"].value;
                var y = document.forms["reg-form"]["password"].value;
                var z = document.forms["reg-form"]["con-password"].value;

                var atpos = x.indexOf("@");
                var dotpos = x.lastIndexOf(".");



                if (w == null || w == "")
                {
                    alert("Enter Your Username");
                    return false;
                }


                else if (atpos < 1 || (dotpos < atpos + 2) || dotpos + 2 >= x.length)
                {
                    alert("Enter a valid e-mail address");
                    return false;
                }

                else if (y == null || y == "")
                {
                    alert("Enter Password");
                    return false;
                }
                else if (z == null || z == "")
                {
                    alert("Confirm Password");
                    return false;
                }
                else if (y != z)
                {
                    alert("Password doesn't match!!");
                    //document.reg-form.y.value = "";
                    //document.reg-form.z.value = "";
                    //document.reg-form.y.focus();
                    return false;
                }
            }
        </script>

    </head><body>
        <div class="main">
            <div class="header">
                <div class="logo">
                    <a href="showrss.php"> <img src="includes/logo2.png" />
                    </a>

                </div>
                <?php
                session_start();

                if (ISSET($_SESSION ['username'])) {
                    echo "<div class='logged'>";

                    echo "<div id='logname'>";
                    // echo "<a href=''>$_SESSION['username']</a>";

                    echo "Hello,";

                    echo "<span style='color:8F3A9E'>";
                    echo $_SESSION ['username'];
                    echo "</span>";
                    echo "</div>";
                    echo "<div id='logout'>";
                    echo "<a href='logout.php'>Logout</a>";
                    echo "</div>";
                    // echo "</div>";
                } else {
                    echo "<div class='login'>";
                    echo "<div id='log'>";
                    echo "<a href='#log'>Login</a>";
                    echo "</div>";
                    echo "<div id='reg'>";
                    echo "<a href='#reg'>Register</a>";
                    echo "</div>";
                    // echo "</div>";
                }
                ?>
            </div>
        </div>

        <div id="___popup"></div>

        <div class="mainmenu" style="width:100%">
            <ul>
                <li class = 'mli'><a href="schedule.php">Schedule</a></li>
                <li class = 'mli'> <a href="feeds.php">Feeds</a></li>
                <li class = 'mli'><a href="browse.php">Browse</a></li>
                <li class = 'mli'><a href="faq.php">F.A.Q</a></li>
                <li class = 'mli'><a href="contact.php">Contact us</a></li>
            </ul>
        </div>

        <div id="panel">

            <form name="login-form" onsubmit="return validateLogin()"
                  action="check_login.php" method="post">
                <input class="username" type="text" name="username"
                       placeholder="Username"><br />
                <p id="err-user">Please Enter your username


                <p>
                    <input class="password" type="password" name="passwd"
                           placeholder="Password"><br />


                <p id="err-pass">Please Enter your password


                <p>

                    <input class="submit" type="submit" value="Login"> <a
                        class="forpass" href="">Forgot Password?</a>

            </form>
        </div>

        <div id="panel2">

            <form name="reg-form" onsubmit="return validateReg()"
                  action="registration.php" method="post">
                <input class="username" type="text" name="username"
                       placeholder="Username"><br /> <input class="email" type="text"
                       name="email" placeholder="Email Address"><br /> <input
                       class="password" type="password" name="password"
                       placeholder="Password"><br /> <input class="con-password"
                       type="password" name="con-password" placeholder="Confirm Password"><br />

                <input class="create-account" type="submit" value="Create Account">
            </form>
        </div>
        <?php
        if (ISSET($_SESSION ['username'])) {
            echo "<div class='show_add'>";
            echo "<div class='add_show'>";
            echo "<form action='addShowToList.php' method ='get'>
				<span>Pick a Show:&nbsp</span> <select name='browse_show'
					id='br_box'>
					<option value=''>Pick a show:</option>";

            include ('userAvailableShows.php');

            echo "</select> <input id='show_info' type='submit' class='home_page_btn'
					value='Add To List'>";

            echo "</form id='frm_add_show'>";

            echo "</div>";

            echo "<div class='add_showName'>";
            echo "<form autocomplete='off' id=\"add-show-form\">
				<span> <b>OR</b>,&nbsp Add a Show:&nbsp</span> 
					<input id='showname' class='addShowName' type='text' name='username'
					placeholder='Enter Show Name'>";

            echo "</select> <input id='add_show_btn' type='button' class='home_page_btn'
					value='Add'>";

            echo "</form>";

            echo "</div>";

            echo "</div>";

            echo "<div class='registered_show'>";
            echo "<div class='yourshow'>";
            echo "<h2>&nbsp;My Shows</h2>";
            echo "<ul>";
            include 'yourShowNames.php';
            echo "</ul>";
            echo "</div>";
            echo "<div class='yourshowlist'>";
            include ('yourshowlist.php');
            echo "</div>";

            echo "</div>";
        }
        ?>
        <div class="maincontent">
            <div class="sidebar">
                <h2>Latest Added Shows</h2>
                <ul> 
                    <?php
                    include ('latest_shows.php');
                    ?>
                </ul>
            </div>

            <div class="content">
                <h2>Recently Aired Shows</h2>

                <ul>
                    <?php
                    include ('recently_aired.php');
                    ?>
                </ul>

            </div>

        </div>
        <div class="footer">
            <p align="right"></p>
        </div>
    </body>
</html>
