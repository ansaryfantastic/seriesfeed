<?php
// header ( 'Content-type: application/json' );

ini_set ( 'display errors', 'On' );
error_reporting ( E_ALL );

require_once ("./JavaBridge/java/Java.inc");

/**
 * Return a list of TV Shows whose names are similar to $showName!
 * The list contains an array of a four-element string array!
 * The first element of the four-element string array is an ID unique to the TV Show,
 * The second element is the name of the show,
 * The third element is the description of the show, and
 * The fourth element is the date in which the show first aired!
 *
 * Example:
 *
 * $shows = getSimilarShowNames("Naruto");
 *
 * foreach ($shows as $show){
 * echo "Show ID: $show[0]<br />";
 * echo "Show Name: $show[1]<br /><br />";
 * echo "Show Description: $show[2]<br /><br />";
 * echo "Air Date: $show[3]<br />";
 *
 * echo "<br />";
 * }
 *
 *
 * @param string $showName        	
 * @return array of four-element string array - the list of shows with names similar to $showName
 */
function getSimilarShowNames($showName) {
	$showDataAnalyzer = new java ( "ShowDataAnalyzer" );
	return java_values ( $showDataAnalyzer->getPossibleTVShowsFromTVDB ( $showName ) );
}
function launchServer() {
	// starts the Java server
}

if (ISSET($_POST['showname']))
	$showname = $_POST ['showname'];
else $showname = "The";

$shows = getSimilarShowNames ( $showname );

$len = count($shows);

?>

<p><h3 style="margin:10px auto;text-align:center;color:#0A6578;text-shadow:.2px .2px #000000">&nbsp;&nbsp;&nbsp;&nbsp;The following shows matched your query! Select the shows you want to subscribe to</h3></p>

<?php

echo "<form name='frm_subscription'>";
echo "<div id='show_list'>";


foreach($shows as $show){
	$show_id = $show[0];
	$show_name = $show[1];
	$show_desc = $show[2];
	$show_air_date = $show[3];

	echo "<h3 style=\" color: #FFFFFF\">$show_name</h3>";
	
	echo "<div>";	

	if (empty($show_desc)) 
		echo "<p style=\"text-align:justify;max-width:92%;padding:5px;box-shadow:0px 0px 79px 86px rgb(89, 89, 89);border-radius:5px;background-color:#A0A0A0 ;color:#FFFFFF\">Show description Not available</p>";
	else 
		echo "<p style=\"text-align:justify;max-width:92%;padding:5px;box-shadow:0px 0px 79px 86px rgb(89, 89, 89);border-radius:5px;background-color:#A0A0A0 ;color:#FFFFFF\">$show_desc</p>";
	
	echo "<table style=\"background-color:#CC66FF; margin-top:6px;width: 94%; border:1px solid blue;border-radius:5px;box-shadow:0px 0px 2px 2px #996600;margin-top: 10px\" border=\"0\"><tr>";	
		echo "<td>";
		echo "<input type=\"checkbox\" id=\"$show_id\"name=\"subscription_list\" style=\"margin-left:10px\"> Subscribe</input>";
		echo "</td>";
		echo "<td>";
		echo "<p style=\"float: right;font-style:italic\">";	
		if (empty($show_air_date))
			echo "Air date not avialable</p>";
		else echo "First Episode: $show_air_date</p>";
		echo "</td>";

	echo "</tr></table>";
		
	echo "</div>";
}
?>

</div>
<div style=width:100%; margin:20px></div>
</form>

<div style="text-align:center"> 
<input style='margin-top: 10px; float: center;width:100px;height:30px;padding:10px auto ;background-color: 	#6666CC ' type='submit' class = 'home_page_btn' id='btn_subscribe' value='Subscribe'></input>
</div>