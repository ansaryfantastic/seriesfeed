<html>
	<head>
	
		<meta name="description" content="Helps to organize your TV Shows in an easy and convenient way" />
		<meta name="keywords" content="rss feed, tv feed, tv rss, show rss, tv rss, tv series feed, rss, feed, tv, series,tv serial,automated rss, automatic show downloader,tv show download" />
		<meta name="author" content="TvRSS">
		<meta name="robots" content="index, follow">
		<meta name="revisit-after" content="1 days">
		<title>Series Feed </title>
		<!-- tv shows, rss feed, series feed -->
		<link href="css/style.css" rel="stylesheet" type="text/css"/>
                <script src="js/jquery-1.10.2.js">
		</script>
<script> 
			$(document).ready(function(){
			  $("#log").click(function(){
				$("#panel").slideToggle("slow");
				$("#panel2").slideUp("slow");
			  });
			});
			$(document).ready(function(){
			  $("#reg").click(function(){
				$("#panel2").slideToggle("slow");
				$("#panel").slideUp("slow");
			  });
			});
			
			$(document).ready(function(){
			  $(".username").click(function(){
				
				$("#err-user").slideUp("fast");
			  });
			});
			$(document).ready(function(){
			  $(".password").click(function(){
				
				$("#err-pass").slideUp("fast");
			  });
			});
		</script>
<script>
			function validateLogin()
			{
			var w=document.forms["login-form"]["username"].value;
			var x=document.forms["login-form"]["passwd"].value;
			
			if (w==null || w=="")
			  {
			  $(document).ready(function(){
				$("#err-user").slideDown("fast");
			  
				});
			  return false;
			  
			  }
			
			if (x==null || x=="")
			  {
			  $(document).ready(function(){
				$("#err-pass").slideDown("fast");
			  
				});
			  return false;
			  }
			
			}
		</script>
<script>
			function validateReg()
			{
			var w=document.forms["reg-form"]["username"].value;
			var x=document.forms["reg-form"]["email"].value;
			var y=document.forms["reg-form"]["password"].value;
			var z=document.forms["reg-form"]["con-password"].value;
			
			var atpos=x.indexOf("@");
			var dotpos=x.lastIndexOf(".");
			


			if (w==null || w=="")
			  {
			  alert("Enter Your Username");
			  return false;
			  }
			  
				
			else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
				  {
				alert("Enter a valid e-mail address");
				return false;
			  }
			  
			else if (y==null || y=="")
			  {
			  alert("Enter Password");
			  return false;
			  }
			else if (z==null || z=="")
			  {
			  alert("Confirm Password");
			  return false;
			  }
			else if(y!=z)
			  {
			  alert("Password and Conform Password field must be same!");
			  //document.reg-form.y.value = "";
			  //document.reg-form.z.value = "";
			  //document.reg-form.y.focus();
			  return false;
			  }
			
			}
		</script>
		
	</head>
	
	<body>
		
		<div class="main">
		    <div class= "header">
				<div class="logo">
				<a href="index.php">	<img src="includes/logo2.png" > </a>
				</div>
                        <?php
			session_start();
			
			if (ISSET($_SESSION['username']))
			{
			echo "<div class='logged'>";

				echo "<div id='logname'>";
					//echo "<a href=''>$_SESSION['username']</a>";
					
					echo "Hello,";
					
					echo "<span style='color:8F3A9E'>";
					echo $_SESSION['username'];
					echo "</span>";
				echo "</div>";
				echo "<div id='logout'>";
					echo "<a href='logout.php'>Logout</a>";
				echo "</div>";
			//echo "</div>";	
				}
			else
			{	
			echo "<div class='login'>";
			    echo "<div id='log'>";
					 echo "<a href='#log'>Login</a>";
				echo "</div>";
				echo "<div id='reg'>";
					echo "<a href='#reg'>Register</a>";
				echo"</div>";
			//echo "</div>";
									
			}	

			?>
				
			</div>
				
			</div>
		<div class="mainmenu" style="width:100%">
            <ul>
				<li class = 'mli'><a href="index.php">Home</a></li>
                <li class = 'mli'><a href="schedule.php">Schedule</a></li>
                <li class = 'mli'> <a href="feeds.php">Feeds</a></li>
                <li class = 'mli'><a href="browse.php">Browse</a></li>
                <li class = 'mli'><a href="faq.php">F.A.Q</a></li>
                
            </ul>
        </div>
                    <div id="panel">

			<form name="login-form" onsubmit="return validateLogin()"
				action="check_login.php" method="post">
				<input class="username" type="text" name="username"
					placeholder="Username"><br />
				<p id="err-user">Please Enter your username</p>
				
				
				<p>
					<input class="password" type="password" name="passwd"
						placeholder="Password"><br />
						</p>
				
				
				<p id="err-pass">Please Enter your password
				</p>
				
				
				<p>

					<input class="submit" type="submit" value="Login"> <a
						class="forpass" href="">Forgot Password?</a>
						</p>
			
			</form>
		</div>

		<div id="panel2">

			<form name="reg-form" onsubmit="return validateReg()"
				action="registration.php" method="post">
				<input class="username" type="text" name="username"
					placeholder="Username"><br /> <input class="email" type="text"
					name="email" placeholder="Email Address"><br /> <input
					class="password" type="password" name="password"
					placeholder="Password"><br /> <input class="con-password"
					type="password" name="con-password" placeholder="Confirm Password"><br />

				<input class="create-account" type="submit" value="Create Account">
			</form>
		</div>
			
			
			
			<div class="maincontent">
				<div class="sidebar">
					<h2>Latest Added Shows</h2>
					<ul>
						<li><a href="">Hulk and the Agents of S.M.A.S.H.</a></li>
						<li><a href="">Avengers Assemble (2013)</a></li>
						<li><a href="">Almost Human</a></li>
						<li><a href="">Citizen Khan</a></li>
						<li><a href="">The Escape Artist</a></li>
						<li><a href="">Ambassadors</a></li>
					</ul>
				</div>
				<div class="content">
					<h2 >Recently Aired Shows</h2>
					
					<ul>
						<li><a href="">Hulk and the Agents of S.M.A.S.H.</a></li>
						<li><a href="">Avengers Assemble (2013)</a></li>
						<li><a href="">Almost Human</a></li>
						<li><a href="">Citizen Khan</a></li>
						<li><a href="">The Escape Artist</a></li>
						<li><a href="">Ambassadors</a></li>
					</ul>
					
				</div>
				
			</div>
			<div class="footer">
				<p align="right"></p>
			</div>
		</div>

	</body>
</html>
