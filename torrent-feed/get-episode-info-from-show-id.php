<?php
// header ( 'Content-type: application/json' );

ini_set ( 'display errors', 'On' );
error_reporting ( E_ALL );

require_once ("./JavaBridge/java/Java.inc");

function getEpisodeData($showid){
	$analyzer = new java ( 'ShowDataAnalyzer' );
	return java_values($analyzer->getSeriesInfo($showid, true));
}


if (ISSET($_POST['showid'])){
	$showid = $_POST['showid'];
	$reqid = $_POST['reqid'];
}
else{
    $showid = "82459";
}

$allEpInfo = getEpisodeData($showid);

echo $reqid." ";

$episodes = Array();
for ($i = 0; $i < 50; $i++){
	$episodes[$i] = 0;
}

$maxseasons = 0;

foreach($allEpInfo as $epinfo){
	$sno = $epinfo[1];
	
	if ($sno > $maxseasons){
            $maxseasons = $sno;
        }
	
	$episodes[$sno]++;
}

for ($i = 1; $i <= $maxseasons; $i++){
	echo "$i $episodes[$i] ";
}

?>