import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class Database {
	private static final String USER = "rakib";
	private static final String PASSWORD = "allowaccess";
	private static final String DB_NAME = "torrentfeed";

	private static final String TABLE_SHOWS = "shows";
	private static final String TABLE_EPISODES = "episodes";
	private static final String TABLE_EPISODE_DATA = "ep_data";

	private static final String AUTH = "IAMSURE";

	private static Database db = null;

	public static Database getInstance() throws Exception {
		if (db == null) {
			db = new Database();
		}

		return db;
	}

	private Connection conn = null;
	private Statement stmt = null;

	private Database() throws Exception {
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		conn = (Connection) DriverManager
				.getConnection("jdbc:mysql://localhost/" + DB_NAME + "?"
						+ "user=" + USER + "&password=" + PASSWORD);

		stmt = (Statement) conn.createStatement();
	}

	public void addEpisode(String seriesid, String epid, String sno,
			String epno, String epname, String desc, Date sqlDate)
			throws SQLException {
		String query = "INSERT INTO "
				+ TABLE_EPISODES
				+ " (ep_id, show_id, s_no, ep_no, ep_name, ep_info, air_date) VALUES(?,?,?,?,?,?,?)";

		java.sql.PreparedStatement pStmt = conn.prepareStatement(query);

		pStmt.setInt(1, Integer.parseInt(epid));
		pStmt.setInt(2, Integer.parseInt(seriesid));
		pStmt.setInt(3, Integer.parseInt(sno));
		pStmt.setInt(4, Integer.parseInt(epno));
		pStmt.setString(5, epname);
		pStmt.setString(6, desc);
		if (sqlDate == null) {
			pStmt.setNull(7, java.sql.Types.DATE);
		} else {
			pStmt.setDate(7, sqlDate);
		}

		pStmt.executeUpdate();
		pStmt.close();
	}

	public String getShowName(String seriesId) {
		String showname = null;
		String q = "SELECT show_name from " + TABLE_SHOWS + " WHERE show_id=\""
				+ seriesId + "\"";
		ResultSet res = null;
		try {
			res = stmt.executeQuery(q);
			if (res.next()) {
				showname = res.getString("show_name");
				res.close();
			}
		} catch (SQLException e) {
			if (res != null)
				try {
					res.close();
				} catch (SQLException e1) {
				}
			return null;
		}

		return showname;
	}

	public void addShow(String showId, String showName, String description)
			throws SQLException {
		String query = "INSERT INTO " + TABLE_SHOWS
				+ " (show_id, show_name, show_desc) VALUES(?,?,?)";

		java.sql.PreparedStatement prepStmt = conn.prepareStatement(query);
		prepStmt.setString(1, showId);
		prepStmt.setString(2, showName);
		prepStmt.setString(3, description);

		prepStmt.executeUpdate();
		prepStmt.close();
	}

	public int addEpisode(int showid, int seasonNo, int episodeNo, String date)
			throws SQLException {
		String q = "SELECT ep_id FROM " + TABLE_EPISODES + " WHERE show_id="
				+ showid + " AND s_no=" + seasonNo + " AND ep_no=" + episodeNo;

		ResultSet res = stmt.executeQuery(q);
		if (res.next()) {
			int id = res.getInt("ep_id");
			res.close();
			return id;
		}

		if (date == null) {
			date = "now()";
		}

		String query = "INSERT INTO " + TABLE_EPISODES
				+ "(show_id, s_no, ep_no, date_added) " + "VALUES (" + showid
				+ ", " + seasonNo + ", " + episodeNo + "," + date + ")";

		stmt.execute(query);

		ResultSet rs = stmt.executeQuery("SELECT LAST_INSERT_ID() AS lid");
		rs.next();

		int id = rs.getInt("lid");
		rs.close();

		return id;
	}

	public void addEpisodeData(int episodeId, String quality, String magnetLink) {

		String query = "INSERT INTO " + TABLE_EPISODE_DATA
				+ "(ep_id, quality, magnet_link) VALUES(" + episodeId + ", '"
				+ quality + "', '" + magnetLink + "')";

		try {
			stmt.execute(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void fetchNewEpisodeData() throws SQLException {
		int id = addEpisode(1, 1, 3, "DATE(NOW())");
		addEpisodeData(id, "720p", "?ma12g2net");
	}

	public void createDatabase(String auth) throws Exception {
		if (!auth.matches(AUTH))
			throw new Exception("Invalid authentication key: " + auth);

		final String createShowsTbl = "CREATE TABLE IF NOT EXISTS "
				+ TABLE_SHOWS + "(show_id INT NOT NULL,"
				+ "show_name VARCHAR(50)," + "show_desc TEXT,"
				+ "show_added TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,"
				+ "PRIMARY KEY(show_id))";

		final String createEpisodesTbl = "CREATE TABLE IF NOT EXISTS "
				+ TABLE_EPISODES + "(ep_id INT NOT NULL AUTO_INCREMENT,"
				+ "show_id INT NOT NULL," + "s_no INT NOT NULL,"
				+ "ep_no BIGINT NOT NULL," + "ep_name TEXT, ep_info TEXT,"
				+ "air_date DATE," + "PRIMARY KEY(ep_id),"
				+ "FOREIGN KEY(show_id) REFERENCES " + TABLE_SHOWS
				+ "(show_id), UNIQUE KEY(show_id, s_no, ep_no))";

		final String createEpisodeDataTbl = "CREATE TABLE IF NOT EXISTS "
				+ TABLE_EPISODE_DATA + "(ep_id BIGINT NOT NULL,"
				+ "quality varchar(20),"
				+ "magnet_link varchar(300), file_size int, "
				+ "PRIMARY KEY(magnet_link), "
				+ "FOREIGN KEY(ep_id) REFERENCES " + TABLE_EPISODES
				+ "(ep_id))";

		stmt.execute("DROP TABLE IF EXISTS " + TABLE_EPISODE_DATA);
		stmt.execute("DROP TABLE IF EXISTS " + TABLE_EPISODES);
		stmt.execute("DROP TABLE IF EXISTS " + TABLE_SHOWS);

		stmt.execute(createShowsTbl);
		stmt.execute(createEpisodesTbl);
		stmt.execute(createEpisodeDataTbl);
	}

	public void closeEverything() throws SQLException {
		if (stmt != null) {
			stmt.close();
		}

		if (conn != null) {
			conn.close();
		}
	}

	public static void main(String[] args) {
		try {
			new Database().createDatabase("IAMSURE");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
