import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Crawler {
	public static final String[] QUALITY = { "x264", "480p", "720p", "1080p" };
	private static final String TORRENT_SITE = "http://kickass.to/usearch/";

	private Document search(String query, int i) throws IOException {
		System.out.println("Searching for: " + query);
		Document doc;
		if (i == 1) {
			doc = Jsoup.connect(
					TORRENT_SITE
							+ URLEncoder.encode(query + " category:tv",
									"utf-8")).get();
		} else {
			doc = Jsoup.connect(
					TORRENT_SITE
							+ URLEncoder.encode(query + " category:tv/" + i,
									"utf-8")).get();
		}

		return doc;
	}

	private void fetchData(Document doc, int showid, String showname, int sno,
			int eno, int episodeId, Database db) {
		Elements els = doc.getElementsByTag("table");
		for (Element el : els) {
			if (el.hasClass("data")) {
				Elements rows = el.getElementsByTag("tr");
				for (Element row : rows) {
					if (row.hasAttr("id")) {
						String name = null, magnetLink = null, fileSize, fileAge;

						Elements nameAndLink = row.getElementsByIndexEquals(0);

						for (Element nal : nameAndLink) {
							Elements ela = nal.getElementsByAttribute("title");
							for (Element f : ela) {
								String title = f.attr("title");
								if (title.contains("magnet")) {
									magnetLink = f.attr("href");
								}

							}
							break;
						}

						if (magnetLink != null) {
							int startIndex = magnetLink.indexOf("&dn=") + 4;
							int endIndex = startIndex;

							int len = magnetLink.length();
							for (int i = startIndex; i < len; i++) {
								if (magnetLink.charAt(i) == '&') {
									endIndex = i;
									break;
								}
							}

							name = magnetLink.substring(startIndex, endIndex);
						}

						Elements size = row.getElementsByIndexEquals(1);
						Element elSize = size.get(size.size() - 1);
						fileSize = elSize.text();

						Elements age = row.getElementsByIndexEquals(3);
						Element elAge = age.get(age.size() - 1);
						fileAge = elAge.text();

						String s[] = new String[4];

						s[0] = name;
						s[1] = magnetLink;
						s[2] = fileSize;
						s[3] = fileAge;

						try {
							String[] showInfo = ShowDataAnalyzer
									.getShowInfoFromMagnetLink(s[0], showname);
							if (showInfo != null) {
								try {
									int sn = Integer.parseInt(showInfo[1]);
									int en = Integer.parseInt(showInfo[2]);

									System.out.print(showInfo[0] + "S" + sn
											+ "E" + en + " ");

									if (areEqual(showInfo[0], showname)
											&& sn == sno && en == eno) {
										System.out.println(" OK");
										if (db != null)
											db.addEpisodeData(episodeId,
													showInfo[3], s[1]);
									} else
										System.out.println();

								} catch (NumberFormatException e) {
								}
							}
						} catch (UnsupportedEncodingException e) {
						}
					}
				}

				break;
			}
		}

	}

	private boolean areEqual(String s1, String s2) {
		int l1 = s1.length();
		int l2 = s2.length();

		int i = 0, j = 0;
		while (true) {
			while (i < l1 && Character.isWhitespace(s1.charAt(i))) {
				i++;
			}

			while (j < l2 && Character.isWhitespace(s2.charAt(j))) {
				j++;
			}

			if (i >= l1) {
				while (j < l2) {
					if (!Character.isWhitespace(s2.charAt(j++)))
						return false;
				}

				return true;
			}

			if (j >= l2) {
				while (i < l1) {
					if (!Character.isWhitespace(s1.charAt(i++)))
						return false;
				}

				return true;

			}

			char c1 = Character.toLowerCase(s1.charAt(i));
			char c2 = Character.toLowerCase(s2.charAt(j));

			if (c1 != c2)
				return false;

			i++;
			j++;
		}
	}

	public void fetchEpisodeData(String showname, int showId, int sno, int eno,
			int episodeId, Database db) throws IOException {

		String searchQuery = showname + " S" + (sno < 10 ? "0" + sno : sno)
				+ "E" + (eno < 10 ? "0" + eno : eno);

		try {
			Document doc = search(searchQuery, 1);
			fetchData(doc, showId, showname, sno, eno, episodeId, db);
		} catch (Exception e) {
		}
	}

	public static void main(String[] args) {
		String seriesname = "The Big Bang Theory";

		System.out.println("Test started for : " + seriesname);
		Crawler crawler = new Crawler();
		try {
			crawler.fetchEpisodeData(seriesname, 80379, 7, 14, 123, null);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
