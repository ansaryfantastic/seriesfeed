import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class TorrentFeed {
	public static void main(String[] args) throws Exception {
		Logger lgr = Logger.getLogger(TorrentFeed.class.getName());
		FileHandler fl = new FileHandler("torrent_feed.log", true);
		fl.setFormatter(new SimpleFormatter());
		lgr.addHandler(fl);

		Scanner in = new Scanner(System.in);

		System.out.print("New TV Series Name: ");
		String showName = in.nextLine();

		System.out.print("New TV Series Description: ");
		String description = in.nextLine();

		System.out.print("Max Pages: ");
		int maxPages = in.nextInt();

		try {
//			Database tfdb = new Database();

			if (showName.equalsIgnoreCase("create")) {
//				tfdb.createDatabase("IAMSURE");
				return;
			}

//			int showId = tfdb.addShow(showName, description);

//			Crawler crawler = new Crawler();
//			crawler.fetchEpisodeData(showName, showId, tfdb, maxPages);

//			tfdb.closeEverything();

		} catch (Exception e) {
			lgr.log(Level.SEVERE, e.getMessage(), e);
		}
	}
}
